from rucio.client import Client
from datetime import datetime
from datetime import time
import json
import time

start_time = time.clock()
CLIENT = Client()

test = CLIENT.ping()

with open('analysis_database.json', 'r') as analysis_database_file:
    analysis_database = json.load(analysis_database_file)

start_date = datetime(2010, 3, 25, 0, 0, 0, 0)
end_date = datetime(2025, 8, 5, 0, 0, 0, 0)


quota_warning = 2000.
quota_danger = 6000.

quota_exceptions = {
    'HLRS_lily': 4.,  # factor by which to increase for exceptional analyses
    'HLRS_ZdZd': 3
}

all_datasets = Client.list_datasets_per_rse(CLIENT, 'CERN-PROD_PHYS-HDBS')

print ('Datasets retrieved')
print (time.clock() - start_time, "seconds")

size_database = {}
dataset_database = {}

# init size 0 for every analysis listed in the database
for analysis_key in analysis_database:
    size_database[analysis_database[analysis_key]] = 0
    dataset_database[analysis_database[analysis_key]] = ''

size_database['unknown'] = 0
dataset_database['unknown'] = ''

for dataset in all_datasets:
    date = dataset[u'created_at']
    date_test = (date > start_date) and (date < end_date)

    if date_test:
        found = False
        for analysis_key in analysis_database:
            if analysis_key in dataset[u'name']:
                size_database[analysis_database[analysis_key]
                              ] += dataset[u'bytes'] / 1e9  # store in GB
                dataset_database[analysis_database[analysis_key]
                                 ] += dataset[u'name'] + '\n'
                found = True
                break  # only store in one entry

        if not found:
            size_database['unknown'] += dataset[u'bytes'] / 1e9
            dataset_database['unknown'] += dataset[u'name'] + '\n'

# magic from https://stackoverflow.com/questions/11228812/print-a-dict-sorted-by-values
sorted_database = sorted(((v, k)
                          for k, v in size_database.iteritems()), reverse=True)
sorted_string = '| *Analysis Team* | *Disk Usage* | *Number of Rules Expiring in < 21 Days* |\n'
for (size, name) in sorted_database:
    entry_string = '| ' + name + ' | ' + str(size) + ' GB |'
    exception_factor = quota_exceptions[name] if name in quota_exceptions else 1.
    color = ''
    if size > (quota_danger * exception_factor):
        color = 'RED'
    elif size > (quota_warning * exception_factor):
        color = 'ORANGE'

    with open('dataset_records/expiring_'+name+'.txt', 'r') as expiring_file:
        expiring_rules = expiring_file.readline().rstrip()

    if color == '':
        sorted_string += '| [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/' + \
            name + '.txt][' + name + ']] | ' + \
            str(size) + ' GB | [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/expiring_' + \
            name + '.txt][' + expiring_rules + ' expiring rules]] |\n'
    else:
        sorted_string += '| [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/' + name + '.txt][' + \
            name + ']] | %' + color + '% ' + \
            str(size) + ' GB %ENDCOLOR% | [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/expiring_' + \
            name + '.txt][' + expiring_rules + ' expiring rules]] |\n'
    print(entry_string)


total_size = 0
for analysis in size_database:
    total_size += size_database[analysis]


today = date.today()
d2 = today.strftime("%B %d, %Y")


grid_total_space = 220
sorted_string += '| *TOTAL* | *{:.2f}/{:.2f} Tb* | *{:.2f}* |\n'.format( (total_size/1000), grid_total_space, (100*(total_size/1000)/grid_total_space))
sorted_string += '| *Last update* | *{update}* |  |\n'.format( update = d2 )
print ('Total usage is ' + str(total_size) + ' GB')

for analysis in dataset_database:
    with open('dataset_records/'+analysis+'.txt', 'w') as datasets_file:
        datasets_file.write(dataset_database[analysis])

with open('dataset_records/sizes.txt', 'w') as sizes_file:
    sizes_file.write(sorted_string)

print (time.clock() - start_time, "seconds")

from rucio.client import Client
from datetime import datetime
from datetime import timedelta

import json

CLIENT = Client()

with open('analysis_database.json', 'r') as analysis_database_file:
    analysis_database = json.load(analysis_database_file)

today = datetime.today()
alert_diff = timedelta(days=21)

filters={u'rse_expression': u'CERN-PROD_PHYS-HDBS'}
all_rules = Client.list_replication_rules(CLIENT,filters=filters)

expiry_database = {}
numrules_database = {}
for analysis_key in analysis_database:
    expiry_database[analysis_database[analysis_key]] = ''
    numrules_database[analysis_database[analysis_key]] = 0

expiry_database['unknown'] = ''
numrules_database['unknown'] = 0

count = 0
for rule in all_rules:
    expiry = rule[u'expires_at']
    if expiry is not None:
        diff_to_today = expiry - today
        if diff_to_today < alert_diff:
            count += 1
            found = False
            for analysis_key in analysis_database:
                if analysis_key in rule[u'name']:
                    expiry_database[analysis_database[analysis_key]] += rule[u'name'] + ' expires at ' + str(expiry) + '\n'
                    numrules_database[analysis_database[analysis_key]] += 1
                    found = True
                    break # Only store one entry
            if not found:
                numrules_database['unknown'] += 1
                expiry_database['unknown'] += rule[u'name'] + ' expires at ' + str(expiry) + '\n'

print ('Found ' + str(count) + ' rules expiring in the next 21 days')

for analysis in expiry_database:
    with open('dataset_records/expiring_'+analysis+'.txt', 'w') as expiring_file:
        expiring_file.write(str(numrules_database[analysis])+'\n')
        expiring_file.write(expiry_database[analysis])
